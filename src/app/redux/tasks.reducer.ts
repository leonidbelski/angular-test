import { TASK_ACTION, TaskAction } from './tasks.action';

const initialState = {
  tasks: [],
  task: {}
};

export function TasksReducer(state = initialState, action: TaskAction) {
  switch (action.type) {
    case TASK_ACTION.ADD_TASK:
      return {
        ...state,
        tasks: [...state.tasks, action.payload]
      };
    case TASK_ACTION.UPDATE_TASK:
      return {
        ...state,
        task: {...action.payload}
      };
    default:
      return state;
  }
}
