import { DASHBOARD_ACTION, DashboardsAction } from './dashboards.action';

const initialState = {
  dashboards: [],
  dashboard: {}
};

export function DashboardsReducer(state = initialState, action: DashboardsAction) {
  switch (action.type) {
    case DASHBOARD_ACTION.ADD_DASHBOARD:
      return {
        ...state,
        dashboards: [...state.dashboards, action.payload]
      };
    case DASHBOARD_ACTION.DELETE_DASHBOARD:
      return {
        ...state,
        dashboards: [...state.dashboards.filter(callback => callback.id !== action.payload.id)]
      };
    case DASHBOARD_ACTION.GET_DASHBOARDS:
      return {
        ...state,
        dashboards: [...action.payload]
      };
    case DASHBOARD_ACTION.GET_DASHBOARD:
      return {
        ...state,
        dashboard: {...action.payload}
      };
    default:
      return state;
  }
}
