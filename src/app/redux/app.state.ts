import { Dashboard } from '../models/dashboard.model';
import { Task } from '../models/task.model';

export interface AppState {
  dashboardsPage: {
    dashboards: Dashboard[],
    dashboard: Dashboard
  };

  tasksPage: {
    tasks: Task[]
  };
}
