import { Action } from '@ngrx/store';
import { Task } from '../models/task.model';

export namespace TASK_ACTION {
  export const ADD_TASK = 'ADD_TASK';
  export const UPDATE_TASK = 'UPDATE_TASK';
}

export class AddTask implements Action {
  readonly  type = TASK_ACTION.ADD_TASK;

  constructor(public payload: Task) {

  }
}

export class UpdateTask implements Action {
  readonly  type = TASK_ACTION.UPDATE_TASK;

  constructor(public payload: Task) {

  }
}

export type TaskAction = AddTask |
                         UpdateTask;
