import { Action } from '@ngrx/store';
import { Dashboard } from '../models/dashboard.model';

export namespace DASHBOARD_ACTION {
  export const ADD_DASHBOARD = 'ADD_DASHBOARD';
  export const DELETE_DASHBOARD = 'DELETE_DASHBOARD';
  export const GET_DASHBOARDS = 'GET_DASHBOARDS';
  export const GET_DASHBOARD = 'GET_DASHBOARD';
}

export class AddDashboard implements Action {
  readonly  type = DASHBOARD_ACTION.ADD_DASHBOARD;

  constructor(public payload: Dashboard) {

  }
}

export class DeleteDashboard implements Action {
  readonly  type = DASHBOARD_ACTION.DELETE_DASHBOARD;

  constructor(public payload: Dashboard) {

  }
}

export class GetDashboards implements Action {
  readonly  type = DASHBOARD_ACTION.GET_DASHBOARDS;

  constructor(public payload: Dashboard[]) {

  }
}

export class GetDashboard implements Action {
  readonly  type = DASHBOARD_ACTION.GET_DASHBOARD;

  constructor(public payload: Dashboard) {

  }
}


export type DashboardsAction = AddDashboard
                             | DeleteDashboard
                             | GetDashboards
                             | GetDashboard;
