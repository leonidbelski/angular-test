import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { HttpClientModule } from '@angular/common/http';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardFormComponent } from './components/dashboard-form/dashboard-form.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomeComponent } from './containers/home/home.component';
import { DashboardPageComponent } from './containers/dashboard-page/dashboard-page.component';
import { DashboardSinglePageComponent } from './containers/dashboard-single-page/dashboard-single-page.component';

import { DashboardsReducer } from './redux/dashboards.reducer';
import { TasksReducer } from './redux/tasks.reducer';
import { TaskFormComponent } from './components/task-form/task-form.component';
import { TaskComponent } from './components/task/task.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardFormComponent,
    DashboardComponent,
    HomeComponent,
    DashboardPageComponent,
    DashboardSinglePageComponent,
    TaskFormComponent,
    TaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    StoreModule.forRoot({dashboardsPage: DashboardsReducer, tasksPage: TasksReducer}),
    StoreDevtoolsModule.instrument()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
