import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { AppState } from '../redux/app.state';
import {
  AddTask,
  UpdateTask
} from '../redux/tasks.action';
import { Task } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  static BASE_URL = 'http://localhost:3000/';

  constructor(
    private http: HttpClient,
    private store: Store<AppState>
  ) { }

  addTask(task: Task) {
    this.http.post(TasksService.BASE_URL + 'tasks', task)
      .subscribe((data: Task) => {
        this.store.dispatch(new AddTask(data));
      });
  }

  updateTask(task: Task) {
    this.http.put(TasksService.BASE_URL + 'tasks/' + task.id, task)
      .subscribe((data: Task) => {
        this.store.dispatch(new UpdateTask(data));
        console.log(data);
      });
  }
}
