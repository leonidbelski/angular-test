import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { AppState } from '../redux/app.state';
import { Dashboard } from '../models/dashboard.model';
import {
  AddDashboard,
  GetDashboards,
  DeleteDashboard,
  GetDashboard
} from '../redux/dashboards.action';

@Injectable({
  providedIn: 'root'
})
export class DashboardsService {

  static BASE_URL = 'http://localhost:3000/';

  constructor(
    private http: HttpClient,
    private store: Store<AppState>
  ) { }

  getDashboards(): void {
    this.http.get(DashboardsService.BASE_URL + 'dashboards')
      .subscribe((dashboards: Dashboard[]) => {
        this.store.dispatch(new GetDashboards(dashboards));
      });
  }

  addDashboard(dashboard: Dashboard) {
    this.http.post(DashboardsService.BASE_URL + 'dashboards', dashboard)
      .subscribe((data: Dashboard) => {
        this.store.dispatch(new AddDashboard(data));
      });
  }

  deleteDashboard(dashboard: Dashboard) {
    this.http.delete(DashboardsService.BASE_URL + 'dashboards/' + dashboard.id)
      .subscribe(() => {
        this.store.dispatch(new DeleteDashboard(dashboard));
      });
  }

  getDashboard(id: string): void {
    this.http.get(DashboardsService.BASE_URL + 'dashboards/' + id + '?_embed=tasks')
      .subscribe((data: Dashboard) => {
        this.store.dispatch(new GetDashboard(data));
      });
  }
}
