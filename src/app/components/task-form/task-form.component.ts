import { Component, OnInit, Input } from '@angular/core';
import { TasksService } from '../../service/tasks.service';
import { DashboardsService } from '../../service/dashboards.service';
import { Task } from '../../models/task.model';
import { Dashboard } from '../../models/dashboard.model';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.scss']
})
export class TaskFormComponent implements OnInit {
  statuses: Array<string> = [
    'To Do',
    'In Progress',
    'Done'
  ];
  taskName = '';
  taskStatus = '';

  @Input() dashboard: Dashboard;

  constructor(
    private serviceTasks: TasksService,
    private service: DashboardsService
  ) { }

  ngOnInit() {
  }

  onAddTask() {
    if (this.taskName === '' || this.taskStatus === '') {
      return;
    }

    const task = new Task(
      this.taskName,
      this.taskStatus,
      +this.dashboard.id
    );

    console.log(this.dashboard);

    this.serviceTasks.addTask(task);

    setTimeout(() => this.service.getDashboard((this.dashboard.id).toString()), 100);

    this.taskName = '';
    this.taskStatus = '';
  }
}
