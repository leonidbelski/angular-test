import { Component, OnInit, Input } from '@angular/core';
import { Task } from '../../models/task.model';
import { TasksService } from '../../service/tasks.service';
import { DashboardsService } from '../../service/dashboards.service';
import { Dashboard } from '../../models/dashboard.model';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  statuses: Array<string> = [
    'To Do',
    'In Progress',
    'Done'
  ];
  @Input() task: Task;
  @Input() dashboard: Dashboard;

  constructor(
    private serviceTasks: TasksService,
    private service: DashboardsService
  ) { }

  ngOnInit() {
  }

  selectedStatus(status: string, taskStatus: string) {
    if (status === taskStatus) {
      return true;
    }
  }

  onChange(event: Event) {
    const value = (event.target as HTMLSelectElement).value;

    this.task.setStatus(value);
    this.serviceTasks.updateTask(this.task);
    setTimeout(() => this.service.getDashboard((this.dashboard.id).toString()), 100);
  }
}
