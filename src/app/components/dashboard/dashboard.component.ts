import { Component, Input } from '@angular/core';
import { Dashboard } from '../../models/dashboard.model';
import { DashboardsService } from '../../service/dashboards.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  @Input() dashboard: Dashboard;

  constructor(
    private service: DashboardsService
  ) { }

  onDelete() {
    this.service.deleteDashboard(this.dashboard);
  }
}
