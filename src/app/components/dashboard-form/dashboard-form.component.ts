import { Component } from '@angular/core';
import { Dashboard } from '../../models/dashboard.model';
import { DashboardsService } from '../../service/dashboards.service';

@Component({
  selector: 'app-dashboard-form',
  templateUrl: './dashboard-form.component.html',
  styleUrls: ['./dashboard-form.component.scss']
})
export class DashboardFormComponent {
  name = '';

  constructor(
    private service: DashboardsService
  ) { }

  onAdd() {
    if (this.name === '') {
      return;
    }

    const dashboard = new Dashboard(
      this.name
    );

    this.service.addDashboard(dashboard);

    this.name = '';
  }
}
