import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';

import { Dashboard } from '../../models/dashboard.model';
import { DashboardsService } from '../../service/dashboards.service';
import { AppState } from '../../redux/app.state';
import { Task } from '../../models/task.model';

@Component({
  selector: 'app-dashboard-single-page',
  templateUrl: './dashboard-single-page.component.html'
})
export class DashboardSinglePageComponent implements OnInit {
  dashboard: Dashboard;
  tasks: Task[];
  taskListToDo: Task[] = [];
  taskListInProgress: Task[] = [];
  taskListDone: Task[] = [];
  statusToDo = 'To Do';
  statusInProgress = 'In Progress';
  statusDone = 'Done';
  id = this.route.snapshot.params[`id`];

  constructor(
    private service: DashboardsService,
    private route: ActivatedRoute,
    private store: Store<AppState>
  ) {}

  ngOnInit() {
    this.service.getDashboard(this.id);

    this.store.select('dashboardsPage').subscribe(data => {
      this.dashboard = new Dashboard(data.dashboard.name, data.dashboard.tasks, data.dashboard.id);

      if (this.dashboard.tasks) {
        this.tasks = this.dashboard.tasks.map(item => {
          const task = new Task(item.name, item.status, item.dashboardId, item.id);
          return task;
        });

        this.taskListToDo = this.tasks.filter((callback: any) => callback.status === this.statusToDo);
        this.taskListInProgress = this.tasks.filter((callback: any) => callback.status === this.statusInProgress);
        this.taskListDone = this.tasks.filter((callback: any) => callback.status === this.statusDone);
      }
    });
  }
}
