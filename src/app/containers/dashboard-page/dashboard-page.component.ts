import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from '../../redux/app.state';
import { DashboardsService } from '../../service/dashboards.service';
import { Dashboard } from '../../models/dashboard.model';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html'
})
export class DashboardPageComponent implements OnInit {
  dashboards: Dashboard[];

  constructor(
    private store: Store<AppState>,
    private service: DashboardsService
  ) {}

  ngOnInit() {
    this.store.select('dashboardsPage').subscribe(data => {
      this.dashboards = data.dashboards.map(item => {
        const dashboard = new Dashboard(item.name, item.tasks, item.id);
        return dashboard;
      });
    });
    this.service.getDashboards();
  }
}
