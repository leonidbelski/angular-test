import { Task } from './task.model';

export class Dashboard {
  constructor(
    public name: string,
    public tasks?: Task[],
    public id?: number
  ) {}
}
