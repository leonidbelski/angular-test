export class Task {
  constructor(
    public name: string,
    public status: string,
    public dashboardId?: number,
    public id?: number
  ) {}

  setStatus(status: string) {
    this.status = status;
  }
}
