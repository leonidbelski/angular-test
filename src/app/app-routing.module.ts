import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './containers/home/home.component';
import { DashboardPageComponent } from './containers/dashboard-page/dashboard-page.component';
import { DashboardSinglePageComponent } from './containers/dashboard-single-page/dashboard-single-page.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'dashboards',
    component: DashboardPageComponent
  },
  {
    path: 'dashboards/:id',
    component: DashboardSinglePageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
